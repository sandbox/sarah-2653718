# Roadmap

This file is used for keeping track of plans for the Drupal 8 version
of Test Entity Flag.

Port to Drupal 8 issue: https://www.drupal.org/node/2910264

The module is usable once "Core" tasks are done, but the D7 version
included the features under "Enhanced" here, so ideally we'll get
those working, too.

## Technical tasks

### Core
- ~~rewrite info file as yaml~~
- ~~hook_default_flags -> CMI~~
- ~~remove default flags on uninstall~~
- ~~update default Views~~
- ~~menu route for test term pages (setting this in the View isn't working)~~
- ~~remove unused D7 code~~
- ~~write documentation/help page/README~~

### Enhanced
- ~~flags/view for media entities~~
- ~~settings form + default flag values~~
- VBO action to bulk delete taxonomy terms
- rework devel functions
- add tests

## User stories

## Core

1. ~~As an admin, when I create a node, term or user, I have the option to flag the entity as Test content~~
2. ~~As an admin, I can view a list of all Test content~~
3. ~~As an admin, I can easily delete all Test content~~

## Enhanced

1. ~~As an admin, I can set a test flag on a media entity (image, document, etc)~~
2. ~~As an admin, I can set the default value of the test flag on a per-bundle basis.~~
3. As a developer, if I create content with Devel generate, this is flagged as Test content