<?php

/**
 * @file
 * Contains \Drupal\test_entity_flag\Form\TestEntityFlagConfigForm.
 */

namespace Drupal\test_entity_flag\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class TestEntityFlagConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'test_entity_flag_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);
    $config = $this->config('test_entity_flag.settings');

    $form['help'] = array(
      '#type' => 'item',
      '#markup' => '<p>' . $this->t('Select the entity types or bundles that should be flagged as Test Content by default. Note that this setting only effects new content.') . '</p>',
    );

    // Retrieve bundle types for nodes, taxonomy vocabularies and media.
    $node_types = \Drupal\node\Entity\NodeType::loadMultiple();
    $taxonomy_types = \Drupal\taxonomy\Entity\Vocabulary::loadMultiple();
    $media_types = \Drupal\media_entity\Entity\MediaBundle::loadMultiple();
    $entity_types = ['node' => $node_types, 'taxonomy' => $taxonomy_types, 'media' => $media_types];

    foreach ($entity_types as $name => $types) {
      $titles = array();
      foreach ($types as $machine_name => $val) {
        $titles[$machine_name] = $val->label();
      }
      $form[$name . '_bundles'] = array(
        '#type' => 'checkboxes',
        '#title' => ucfirst($name) . ' ' . $this->t('types'),
        '#options' => $titles,
        '#default_value' => $config->get('test_entity_flag.'. $name .'_bundles'),
      );
    }

    // Users don't have bundles.
    $form['users'] = array(
      '#type' => 'checkboxes',
      '#title' => $this->t('Users'),
      '#options' => ['Users'],
      '#default_value' => $config->get('test_entity_flag.user_bundles'),
    );

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('test_entity_flag.settings');

    $config->set('test_entity_flag.node_bundles', $form_state->getValue('node_bundles'));
    $config->set('test_entity_flag.taxonomy_bundles', $form_state->getValue('taxonomy_bundles'));
    $config->set('test_entity_flag.media_bundles', $form_state->getValue('media_bundles'));
    $config->set('test_entity_flag.users', $form_state->getValue('user_bundles'));

    $config->save();

    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'test_entity_flag.settings',
    ];
  }

}