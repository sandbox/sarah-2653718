<?php
/**
 * @file
 * Code for the Test Entity Flag module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Implements hook_help().
 */
function test_entity_flag_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.test_entity_flag':

      // Retrieve links to module-defined pages.
      $test_content_admin_pages = [
        'Test content' => 'view.test_content.page_1',
        'Test users' => 'view.test_users.page_1',
        'Test terms' => 'entity.taxonomy_vocabulary.collection'
      ];
      // Add Media Entity if applicable.
      $moduleHandler = \Drupal::service('module_handler');
      if ($moduleHandler->moduleExists('media_entity')){
        $test_content_admin_pages['Test media'] = 'view.test_media.page_1';
      }
      // Build list of links.
      foreach ($test_content_admin_pages as $name => $route) {
        $url = Url::fromRoute($route);
        $link = Link::fromTextAndUrl($name, $url);
        $link = $link->toRenderable();
        $link = render($link);
        // A little extra help for taxonomy since we can't link straight to
        // the lists.
        if ($test_content_admin_pages[$name] == 'Test terms') {
          $link = $link . ' (edit a vocabulary to see test terms)';
        }
        $links[] = '<li>' . $link . '</li>';
      }

      // Help page content.
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Creating test content is a standard part of developing a new site, but removing all of that test content before launch can be error-prone and tedious, especially once you\'ve added some real content.</p><p>The <a href=":test_entity_flag_url">Test Entity Flag</a> module adds a checkbox to entities which allows content creators to flag the entity as test content. It also provides an admin UI which can be used to bulk unflag or delete test content items.',
          [
            ':test_entity_flag_url' => 'https://www.drupal.org/sandbox/sarah/2653718',
          ]) . '</p>';

      $output .= '<h3>' . t('Test content lists') . '</h3>';
      $output .= '<ul>';
      $output .= implode('', $links);
      $output .= '</ul>';

      $output .= '<h3>' . t('Uninstalling') . '</h3>';
      $output .= '<p>' . t('On uninstall, all Test Flag data and views will be removed.') . '</p>';

      return $output;
  }
}

/**
 * Implements hook_form_alter().
 */
function test_entity_flag_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {

  $test_flags = ['node', 'taxonomy', 'user', 'media'];
  foreach ($test_flags as $flag) {

    // Check if this form contains a Test Flag checkbox.
    if (isset($form['flag']['test_' . $flag])) {

      // Look up config for this entity type.
      $config = \Drupal::config('test_entity_flag.settings')->get('test_entity_flag.' . $flag . '_bundles');

      // Check which bundle this form is for.
      $bundle = FALSE;
      if ($form_object = $form_state->getFormObject()) {
        if ($form_object instanceof Drupal\Core\Entity\ContentEntityForm) {
          $bundle = $form_object->getEntity()->bundle();
        } elseif ($form_object instanceof Drupal\field_ui\Form\FieldConfigEditForm) {
          $bundle = $form_object->getEntity()->bundle();
        }
      }
      
      // If configured as such, flag it Test Content by default.
      if (isset($config[$bundle]) && $config[$bundle] !== 0) {
        $form['flag']['test_' . $flag]['#default_value'] = TRUE;
      }

      // Pop open the Flags fieldset.
      $form['flag']['#open'] = TRUE;

      // Quit the loop, we'll never find multiple entity type flags.
      break;

    }
  }

}
